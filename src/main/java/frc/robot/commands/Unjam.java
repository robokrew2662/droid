package frc.robot.commands;

import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.CentererSubsystem;
import frc.robot.subsystems.IndexterSubsystem;
import frc.robot.subsystems.IntakeSubsystem;

/**
 * A command to drive the robot with joystick input (passed in as {@link DoubleSupplier}s). Written
 * explicitly for pedagogical purposes - actual code should inline a command this simple with {@link
 * edu.wpi.first.wpilibj2.command.RunCommand}.
 */
public class Unjam extends CommandBase {
    private final IndexterSubsystem m_index;
    private final IntakeSubsystem m_intake;
    private final CentererSubsystem m_center;

    public Unjam(IndexterSubsystem indexter, IntakeSubsystem intake, CentererSubsystem center) {
    m_index = indexter;
    m_intake = intake;
    m_center = center;
    addRequirements(m_index);
    addRequirements(m_intake);
    addRequirements(m_center);
    }

    @Override
    public void execute() {
        
        m_index.reverse();
        m_intake.reverseIntake();
        m_center.reverseCenterer();

    }

    @Override
    public boolean isFinished()
    {
        return true;
    }


}