package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.IntakeSubsystem;

public class IntakeOn extends CommandBase {
    private final IntakeSubsystem m_intakeSubsystem;

    public IntakeOn(IntakeSubsystem subsystem) {
        m_intakeSubsystem = subsystem;
        addRequirements(m_intakeSubsystem);

    }
    @Override
    public void execute() {
        m_intakeSubsystem.turnOnIntake();

    }
    @Override
    public boolean isFinished(){
        return true;
    }
}



