/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.commands.Autonomous;
import frc.robot.commands.CentererOff;
import frc.robot.commands.CentererOn;
import frc.robot.commands.ChangeShooterState;
import frc.robot.commands.DefaultDrive;
import frc.robot.commands.IndexterOff;
import frc.robot.commands.IndexterOn;
import frc.robot.commands.IntakeOff;
import frc.robot.commands.IntakeOn;
import frc.robot.commands.PullInIntake;
import frc.robot.commands.ReleaseIntake;
import frc.robot.commands.ReverseDriveOff;
import frc.robot.commands.ReverseDriveOn;
import frc.robot.commands.ShooterSpeedDecrease;
import frc.robot.commands.ShooterSpeedIncrease;
import frc.robot.commands.ShooterSpeedMax;
import frc.robot.commands.TurnOffWinch;
import frc.robot.commands.Unjam;
import frc.robot.subsystems.CentererSubsystem;
import frc.robot.subsystems.DriveSubsystem;
import frc.robot.subsystems.IndexterSubsystem;
import frc.robot.subsystems.IntakeSubsystem;
import frc.robot.subsystems.ShooterSubsystem;
import frc.robot.trigger.TriggerAsButton;

/**
 * This class is where the bulk of the robot should be declared.  Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls).  Instead, the structure of the robot
 * (including subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {


  // The robot's subsystems and commands are defined here... 
  
  //private final DriveSubsystem m_robotDrive = new DriveSubsystemSim();
  
  private final DriveSubsystem m_robotDrive = new DriveSubsystem();
  

  private final IndexterSubsystem m_indexter = new IndexterSubsystem();
  private final XboxController m_joystick = new XboxController(Constants.DRIVE_CONTROL_PORT);
  private final XboxController m_secondaryController = new XboxController(Constants.OPERATOR_CONTROL_PORT);
  private final IntakeSubsystem m_intakeSubsystem = new IntakeSubsystem();
  private final ShooterSubsystem m_shooterSubsystem = new ShooterSubsystem();
  private final CentererSubsystem m_centererSubsystem = new CentererSubsystem();



  /**
   * The container for the robot.  Contains subsystems, OI devices, and commands.
   */
  public RobotContainer() {

    m_robotDrive.driveSubInit();
    configureButtonBindings();
    m_robotDrive.setDefaultCommand( new DefaultDrive(() -> m_joystick.getRawAxis(Constants.DRIVE_Y_AXIS),() -> m_joystick.getRawAxis(Constants.DRIVE_X_AXIS), m_robotDrive));


  }

  /**
   * Use this method to define your button->command mappings.  Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a
   * {@link edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {

    final JoystickButton yButton = new JoystickButton(m_secondaryController, XboxController.Button.kY.value);
    final JoystickButton aButton = new JoystickButton(m_secondaryController, XboxController.Button.kA.value);
    final JoystickButton bButton = new JoystickButton(m_secondaryController, XboxController.Button.kB.value);
    final JoystickButton xButton = new JoystickButton(m_secondaryController, XboxController.Button.kX.value);
    final JoystickButton rightBumper = new JoystickButton(m_secondaryController, XboxController.Button.kBumperRight.value);
    final JoystickButton leftBumper = new JoystickButton(m_secondaryController, XboxController.Button.kBumperLeft.value);
    int leftTriggerAxis;
    int rightTriggerAxis;
    final JoystickButton driveAButton = new JoystickButton(m_joystick, XboxController.Button.kA.value);
    final JoystickButton driveBButton = new JoystickButton(m_joystick, XboxController.Button.kB.value);
    final JoystickButton driveLBumper =  new JoystickButton(m_joystick, XboxController.Button.kBumperRight.value);


    if(Robot.isReal())
    {
      leftTriggerAxis = Constants.LEFT_TRIGGER_AXIS;
      rightTriggerAxis = Constants.RIGHT_TRIGGER_AXIS;
    }
    else
    {
      leftTriggerAxis = Constants.SIM_LEFT_TRIGGER_AXIS;
      rightTriggerAxis = Constants.SIM_RIGHT_TRIGGER_AXIS;
    }

    final TriggerAsButton rTrigger = new TriggerAsButton(m_secondaryController, rightTriggerAxis);
    final TriggerAsButton lTrigger = new TriggerAsButton(m_secondaryController, leftTriggerAxis);
    
    xButton.whenHeld(new CentererOn(m_centererSubsystem));
    xButton.whenHeld(new IntakeOn(m_intakeSubsystem));
    xButton.whenReleased(new CentererOff(m_centererSubsystem));
    xButton.whenReleased(new IntakeOff(m_intakeSubsystem));
    rTrigger.whenHeld(new IndexterOn(m_indexter));
    rTrigger.whenReleased(new IndexterOff(m_indexter));
    leftBumper.whenPressed(new ShooterSpeedDecrease(m_shooterSubsystem));
    rightBumper.whenPressed(new ShooterSpeedIncrease(m_shooterSubsystem));
    yButton.whenPressed(new ShooterSpeedMax(m_shooterSubsystem));
    aButton.whenPressed(new ChangeShooterState(m_shooterSubsystem));
    bButton.whenHeld(new Unjam(m_indexter, m_intakeSubsystem, m_centererSubsystem));
    bButton.whenReleased(new IntakeOff(m_intakeSubsystem));
    bButton.whenReleased(new IndexterOff(m_indexter));
    bButton.whenReleased(new CentererOff(m_centererSubsystem));
    driveAButton.whenHeld(new ReleaseIntake(m_intakeSubsystem));
    driveAButton.whenReleased(new TurnOffWinch(m_intakeSubsystem));
    driveBButton.whenHeld(new PullInIntake(m_intakeSubsystem));
    driveBButton.whenReleased(new TurnOffWinch(m_intakeSubsystem));
    driveLBumper.whenHeld(new ReverseDriveOn(m_robotDrive));
    driveLBumper.whenReleased(new ReverseDriveOff(m_robotDrive));

    
  }

  public Command getAutonomousCommand() {
    return new Autonomous(m_robotDrive, m_intakeSubsystem, m_indexter, m_shooterSubsystem, m_centererSubsystem);
  }


}
