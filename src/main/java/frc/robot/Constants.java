/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide
 * numerical or boolean constants. This class should not be used for any other
 * purpose. All constants should be declared globally (i.e. public static). Do
 * not put anything functional in this class.
 *
 * <p>
 * It is advised to statically import this class (or one of its inner classes)
 * wherever the constants are needed, to reduce verbosity.
 */
public final class Constants {
    /* Current threshold to trigger current limit */
    public static final int PEAK_MOTOR_AMPS = 40;
    
    /* Duration after current exceed Peak Current to trigger current limit */
    public static final int LENGTH_AT_PEAK = 0;

    /* Current to mantain once current limit has been triggered */
    public static final int MAINTAIN_AMPS = 30;

    /**
     * Timeout value generally used in parameter configs
     * Non-zero to block the config until success, zero to skip checking 
     */
    public static final int CONFIG_TIMEOUT = 0;
        
    public static boolean CURRENT_LIMITING_ENABLED = true;

    private static double BALL_INSIDE_TIME = 0.5; // in seconds
    public static final double BALL_INSIDE_LOOP_MAX = BALL_INSIDE_TIME * 1000 ; // in milliseconds

    public static double AUTO_DRIVE_TIME_MAX = 10.0; // in seconds
    public static final double AUTO_DRIVE_LOOP_MAX = (AUTO_DRIVE_TIME_MAX*1000) / 20;



    //Motor speeds
    public static double DRIVE_MOTOR_MAXCHANGE_SPEED = .3;
    public static double CLIMBER_MOTOR_SPEED = .5;
    public static double SHOOTER_DESCREASE_SPEED = 0.1;
    public static double SHOOTER_INCREASE_SPEED = -0.1;
    public static double CENTER_MOTOR_SPEED = 0.5;
    public static double INDEXTER_MOTOR_SPEED = 0.3;
    public static double INTAKE_MOTOR_SPEED = 0.5;
    public static double WINCH_MOTOR_SPEED = 0.5;
    public static double AUTO_DRIVE_SPEED = 1.0;
    public static double SHOOTER_MAX_SPEED = -0.63;
    
    //Motor Numbers
    public static int LEFT_DRIVE_MOTOR_MASTER_CAN_ID = 3;
    public static int RIGHT_DRIVE_MOTOR_MASTER_CAN_ID = 4;
    public static int LEFT_DRIVE_MOTOR_SLAVE1_CAN_ID = 1;
    public static int LEFT_DRIVE_MOTOR_SLAVE2_CAN_ID = 2;
    public static int RIGHT_DRIVE_MOTOR_SLAVE1_CAN_ID = 5;
    public static int RIGHT_DRIVE_MOTOR_SLAVE2_CAN_ID = 6;
    public static int SHOOTER_UPPER1_PWM_PORT = 5;
    public static int SHOOTER_LOWER1_PWM_PORT = 2;
    public static int SHOOTER_LOWER1_CAN_ID = 7;
    public static int INDEX_PWM_PORT = 3;
    public static int INTAKE_PWM_PORT = 1;
    public static int CENTERER_PWM_PORT = 0;
    public static int WINCH_PWM_PORT = 4;
    public static int CLIMBER_PWM_PORT = 6;

    //Sensor
    public static int UPPER_INDEX_IO_PORT = 1;
    public static int LOWER_INDEX_IO_PORT = 0;

    //Drive Controler
    public static int DRIVE_CONTROL_PORT = 0;
    public static int DRIVE_Y_AXIS = 4;
    public static int DRIVE_X_AXIS = 1;

    //Operator Controller
    public static int OPERATOR_CONTROL_PORT = 1;
    public static int LEFT_TRIGGER_AXIS = 2;
    public static int RIGHT_TRIGGER_AXIS = 3;
    public static int SIM_LEFT_TRIGGER_AXIS = 4;
    public static int SIM_RIGHT_TRIGGER_AXIS = 5;

}
