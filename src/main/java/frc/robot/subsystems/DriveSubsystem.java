/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;

import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class DriveSubsystem extends SubsystemBase {

    /* Master Talons for arcade drive */
    WPI_TalonSRX _frontLeftMotor = new WPI_TalonSRX(Constants.LEFT_DRIVE_MOTOR_MASTER_CAN_ID);
    WPI_TalonSRX _frontRightMotor = new WPI_TalonSRX(Constants.RIGHT_DRIVE_MOTOR_MASTER_CAN_ID);

    /* Follower Talons + Victors for six motor drives */
    WPI_VictorSPX _leftSlave1 = new WPI_VictorSPX(Constants.LEFT_DRIVE_MOTOR_SLAVE1_CAN_ID);
    WPI_VictorSPX _leftSlave2 = new WPI_VictorSPX(Constants.LEFT_DRIVE_MOTOR_SLAVE2_CAN_ID);
    WPI_VictorSPX _rightSlave1 = new WPI_VictorSPX(Constants.RIGHT_DRIVE_MOTOR_SLAVE1_CAN_ID);
    WPI_VictorSPX _rightSlave2 = new WPI_VictorSPX(Constants.RIGHT_DRIVE_MOTOR_SLAVE2_CAN_ID);
    double curJoyValue;
    double driveCounter;
    boolean reverseDrive = false;

    DifferentialDrive robotDrive = new DifferentialDrive(_frontLeftMotor, _frontRightMotor);

    public DriveSubsystem() {
        super();
    }

    public void driveSubInit() {

        curJoyValue = 0;
        _frontLeftMotor.configFactoryDefault();
        _frontRightMotor.configFactoryDefault();
        _leftSlave1.configFactoryDefault();
        _leftSlave2.configFactoryDefault();
        _rightSlave1.configFactoryDefault();
        _rightSlave2.configFactoryDefault();

        _frontLeftMotor.configPeakCurrentLimit(Constants.PEAK_MOTOR_AMPS, Constants.CONFIG_TIMEOUT);
        _frontLeftMotor.configPeakCurrentDuration(Constants.LENGTH_AT_PEAK, Constants.CONFIG_TIMEOUT);
        _frontLeftMotor.configContinuousCurrentLimit(Constants.MAINTAIN_AMPS, Constants.CONFIG_TIMEOUT);
        _frontLeftMotor.enableCurrentLimit(Constants.CURRENT_LIMITING_ENABLED);

        _frontRightMotor.configPeakCurrentLimit(Constants.PEAK_MOTOR_AMPS, Constants.CONFIG_TIMEOUT);
        _frontRightMotor.configPeakCurrentDuration(Constants.LENGTH_AT_PEAK, Constants.CONFIG_TIMEOUT);
        _frontRightMotor.configContinuousCurrentLimit(Constants.MAINTAIN_AMPS, Constants.CONFIG_TIMEOUT);
        _frontRightMotor.enableCurrentLimit(Constants.CURRENT_LIMITING_ENABLED);

        /**
         * Take our extra motor controllers and have them
         * follow the Talons updated in arcadeDrive 
         */
        _leftSlave1.follow(_frontLeftMotor);
        _leftSlave2.follow(_frontLeftMotor);
        _rightSlave1.follow(_frontRightMotor);
        _rightSlave2.follow(_frontRightMotor);

        /**
         * Drive robot forward and make sure all motors spin the correct way.
         * Toggle booleans accordingly.... 
         */
        _frontLeftMotor.setInverted(true); // <<<<<< Adjust this until robot drives forward when stick is forward
        _frontRightMotor.setInverted(true); // <<<<<< Adjust this until robot drives forward when stick is forward
        _leftSlave1.setInverted(true);
        _leftSlave2.setInverted(true);
        _rightSlave1.setInverted(true);
        _rightSlave2.setInverted(true);

        /* diff drive assumes (by default) that 
			right side must be negative to move forward.
      Change to 'false' so positive/green-LEDs moves robot forward  */

    }

    @Override
    public void periodic() {
        // This method will be called once per scheduler run
    }

    public void reverseDriveOn() {
        reverseDrive = true;
    }

    public void reverseDriveOff() {
        reverseDrive = false;
    }

    public void directDrive(double y, double x) {
        robotDrive.arcadeDrive(y, -x, false);

    }
    public void drive(double y, double x) {

        if (reverseDrive) {
            y = -y;
            x = -x;
        }

        double newDrivePos = y; //desired
        double speedChange = newDrivePos - curJoyValue;
        if (newDrivePos != curJoyValue) {
            if (speedChange >= Constants.DRIVE_MOTOR_MAXCHANGE_SPEED) {
                speedChange = Constants.DRIVE_MOTOR_MAXCHANGE_SPEED;
            } else if (speedChange <= -Constants.DRIVE_MOTOR_MAXCHANGE_SPEED) {
                speedChange = -Constants.DRIVE_MOTOR_MAXCHANGE_SPEED;
            }
        }

        curJoyValue = curJoyValue + speedChange;
        robotDrive.arcadeDrive(curJoyValue, -x, false);

    }
}
