package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class ShooterSubsystem extends SubsystemBase {
    VictorSP shooterUpper1 = new VictorSP(Constants.SHOOTER_UPPER1_PWM_PORT);
    VictorSP shooterLower1 = new VictorSP(Constants.SHOOTER_LOWER1_PWM_PORT);
    VictorSPX shooterLower2 = new VictorSPX(Constants.SHOOTER_LOWER1_CAN_ID);
    public double newSpeed = 0.0;

    public void setSpeed(double speedInput) {
    
    double curSpeed  = shooterLower1.get();
    
    
    if(speedInput < 0) 
    {
        if(curSpeed + speedInput < -0.63){
            newSpeed = -0.63;
        }
        else
        {
            newSpeed = curSpeed + speedInput;
        }
        
    }
    else if(speedInput > 0) {
        
        if(curSpeed + speedInput > 0.0){
            newSpeed = 0.0;
        }
        else
        {
            newSpeed =  curSpeed + speedInput;
        }
        
        
    }

    shooterUpper1.set(newSpeed);
    shooterLower1.set(newSpeed);
    shooterLower2.set(ControlMode.PercentOutput, newSpeed);
        
    }

    public void turnOnShooter(double SpeedThing) {
        shooterUpper1.set(SpeedThing);
        shooterLower1.set(SpeedThing);
        shooterLower2.set(ControlMode.PercentOutput, SpeedThing);
    }

    public void turnOffShooter() {
        shooterUpper1.set(0);
        shooterLower1.set(0);
        shooterLower2.set(ControlMode.PercentOutput, 0);
    }

    public double getSpeed()
    {
        return shooterUpper1.get();
    }
}