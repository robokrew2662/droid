package frc.robot.subsystems;

import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class IntakeSubsystem extends SubsystemBase {
    VictorSP intakeMotor = new VictorSP(Constants.INTAKE_PWM_PORT);
    VictorSP intakeWinch = new VictorSP(Constants.WINCH_PWM_PORT);

    public void turnOnIntake() {
        intakeMotor.set(-Constants.INTAKE_MOTOR_SPEED);
    }

    public void turnOffIntake() {
        intakeMotor.set(0);
    }

    public void reverseIntake() {
        intakeMotor.set(Constants.INTAKE_MOTOR_SPEED);
    }

    public void pullInIntake() {
        intakeWinch.set(Constants.WINCH_MOTOR_SPEED);
    }

    public void releaseIntake() {
        intakeWinch.set(Constants.WINCH_MOTOR_SPEED);
    }

    public void turnOffWinch() {
        intakeWinch.set(0);
    }
}