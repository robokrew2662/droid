package frc.robot.subsystems;

import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class ClimberSubsystem extends SubsystemBase {
    VictorSP climberMotor = new VictorSP(Constants.CLIMBER_PWM_PORT);

    public void turnOnClimber() {
        climberMotor.set(Constants.CLIMBER_MOTOR_SPEED);
    }

    public void turnOffClimber() {
        climberMotor.set(0);
    }

    public void reverseClimber() {
        climberMotor.set(-Constants.CLIMBER_MOTOR_SPEED);
    }
}