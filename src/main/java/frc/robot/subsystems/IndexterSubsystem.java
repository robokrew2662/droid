/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class IndexterSubsystem extends SubsystemBase {
  

  
  private final VictorSP indexerMotor = new VictorSP(Constants.INDEX_PWM_PORT);
  private final DigitalInput lowerIntake = new DigitalInput(Constants.LOWER_INDEX_IO_PORT);
  private final DigitalInput upperIntake = new DigitalInput(Constants.UPPER_INDEX_IO_PORT);
  private Integer ballCounter = 0;
  private Integer ballInsideClock = 0;
  private Integer ballOutsideClock = 0;
  private boolean motorState = false;

  public IndexterSubsystem() {
    super();

  }
 
  public boolean  ballInside() {

    boolean ballInside = false;

     if (lowerIntake.get()){
       if (ballInsideClock < Constants.BALL_INSIDE_LOOP_MAX)
       {
         ballInsideClock++;
       }
       else
       {
         ballInside = true;
         ballCounter++;
       }
     }

     return ballInside;

  }

  public boolean  oneBallOut() {

    boolean ballOut = false;

     if (upperIntake.get()){
       if (ballOutsideClock < Constants.BALL_INSIDE_LOOP_MAX)
       {
        ballOutsideClock++;
       }
       else
       {
          ballOut = true;
          ballCounter--;
       }
     }

     return ballOut;

  }

  public boolean  allBallOut() {

    boolean allBallOut = false;

     if (upperIntake.get()){
       if (ballOutsideClock < Constants.BALL_INSIDE_LOOP_MAX)
       {
        ballOutsideClock++;
       }
       else
       {
          ballCounter--;
          ballOutsideClock = 0;
       }
     }

     allBallOut = (ballCounter == 0);
     return allBallOut;

  }

  public boolean  getHigher() {
  
    return upperIntake.get();
  }

  
  public void turnMotorOn() {

        indexerMotor.set(-Constants.INDEXTER_MOTOR_SPEED);
  }

  public void reverse() {

    indexerMotor.set(Constants.INDEXTER_MOTOR_SPEED);
}

  public boolean getMotorState()
  {
    return motorState;
  }

  public int getBallCount()
  {
    return ballCounter;
  }
  public void turnMotorOff() {

      indexerMotor.set(0);
   
  }
}
