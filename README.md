## Controls

### Drive Controller
- Left Analog: Forward/Backward
- Right Analog: Left/Right
- LB (Held): Reverse drive ON
- LB (Released): Reverse drive OFF

### Secondary Controller
- A: Toggle shooter state
- B: Unjam
- X (Held): Intake + centerer ON
- X (Released): Intake + centerer OFF
- Y: Shooter at highest speed
- LB: Decrement shooter speed
- RT (Held): Indexer ON
- RT (Released): Indexer OFF
- RB: Increment shooter speed
